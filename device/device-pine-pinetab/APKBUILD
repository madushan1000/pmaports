# Reference: <https://postmarketos.org/devicepkg>
pkgname="device-pine-pinetab"
pkgdesc="Pine64 Pinetab"
pkgver=0.1
pkgrel=6
url="https://postmarketos.org"
license="MIT"
arch="aarch64"
options="!check !archcheck"
depends="postmarketos-base linux-postmarketos-allwinner u-boot-pine64 uboot-tools mesa-git mesa-git-glapi mesa-git-gl mesa-git-egl mesa-git-dri-lima mesa-git-dri-kmsro mesa-git-gbm mesa-git-gles"
makedepends="devicepkg-dev"
subpackages="$pkgname-sway
             $pkgname-weston
             $pkgname-nonfree-firmware:nonfree_firmware"
install="$pkgname.post-install"
source="
	deviceinfo
	uboot-script.cmd
	sway.conf
	99-pinetab-keyboard.hwdb
	weston.ini
"

build() {
	devicepkg_build $startdir $pkgname
	mkimage \
		-A arm \
		-O linux \
		-T script \
		-C none \
		-a 0 \
		-e 0 \
		-n postmarketos \
		-d "$srcdir"/uboot-script.cmd \
		"$srcdir"/boot.scr
}

sway() {
        install_if="$pkgname postmarketos-ui-sway"
        depends="dmenu"
        install -D -m644 "$srcdir"/sway.conf \
                "$subpkgdir"/etc/sway/config.d/pinetab
}

weston() {
        install_if="$pkgname postmarketos-ui-weston"
        install -Dm644 "$srcdir"/weston.ini \
                "$subpkgdir"/etc/xdg/weston/weston.ini
}

package() {
	devicepkg_package $startdir $pkgname

	# U-Boot
	install -Dm644 "$srcdir"/boot.scr \
		"$pkgdir"/boot/boot.scr

	# Fix for the meta key on v1 keyboard dock, probably not needed on newer revisions
	install -Dm644 "$srcdir"/99-pinetab-keyboard.hwdb \
		"$pkgdir"/etc/udev/hwdb.d/99-pinetab-keyboard.hwdb
}

nonfree_firmware() {
	pkgdesc="Bluetooth Firmware"
	depends="firmware-rtl8723bt"
	mkdir "$subpkgdir"
}

sha512sums="78b3414f198f2efbfcb12d6240e26a34cd48f00f75a0469c57c372a84aaac75a8f5ddd23f8cb1c16b9a9de6b2a11a8f537a1cd486f75dfbce213541f6890211b  deviceinfo
8f15bb62704ad5379c6f77c5ea766e69587a11829539cc339b486b72e248e04cc6202b505f12846f6537f259412f7749ce50d0b15227da182afe17fe7dd303e5  uboot-script.cmd
9e71c61bfa72b60e749c4dc04f17eb59d25de822765e81e65c937d6be5236d3951a652b69a1ab973abe6add9f34705be5dbaa98bbfa25daf7504dd50b28524c2  sway.conf
8d1ef04911f1cfa7eda8185f4f4d807af23105a7c8f23d8c18f02afeccd8ab124de70c2e2760c154a3128fe3793447039fb0abf37aa496f597d27051e275033e  99-pinetab-keyboard.hwdb
6374ef977eab14b71fa88057ebd52ca03035e387e5f41504598a990f24b2e92bccd2db476c545dc541000e08fccdecb6f1cd63e25b5835df706269e0384dacc7  weston.ini"
